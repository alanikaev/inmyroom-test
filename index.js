import { DaData } from './lib/dadata.js'
import { Autocomplete } from './lib/autocomplete.js'

const dadata_token = '6c6f2a5513821eca5e2b9a256077f39ade403db7';
const dadata_baseUrl = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs';
const dadataClient = new DaData(dadata_token, dadata_baseUrl);

const inputNode = document.querySelector('.autocomplete-input');
const resultNode = document.querySelector('.autocomplete-result');

const daDataSearcher = (query) => {
	dadataClient.find(query);

	return dadataClient.result;
}

const autocomplete = new Autocomplete(inputNode, resultNode, daDataSearcher);
