export class DaData {
	constructor(token, baseUrl) {
		this.token = token;
		this.baseUrl = baseUrl;

		this.client = axios.create({
			baseURL: this.baseUrl,
			timeout: 5000,
			headers: {
				'Content-Type': 'application/json',
    			'Accept': 'application/json',
    			'Authorization': `Token ${this.token}`
			}
		});
	};

	find(query) {
		this.client.post('/suggest/address', {
			query: query,
			from_bound: { value: 'city' },
			to_bound: { value: 'city' },
			count: 5
		})
		.then(response => {
			this.result = response.data.suggestions.map(({ data }) => (data.city))
		})
		.catch(error => {
			// Костыль
			this.result = []
		})
	};
}


