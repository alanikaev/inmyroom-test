export class Autocomplete {
	constructor(inputNode, resultNode, searcher) {
		this.inputNode = inputNode;
		this.resultNode = resultNode;
		this.searcher = searcher;
		this.result = [];

		this.inputNode.addEventListener('input', this.debounce(this.handleInput));
		this.resultNode.addEventListener('click', this.handleResultClick);
	}

	handleInput = ({ target }) => {
		this.clearResult();

		let inputValue = target.value;

		if (inputValue.length > 0) {
			this.result = this.searcher(inputValue);

			this.addElementsTo(this.resultNode);
		}
	}

	handleResultClick = ({ target }) => {
		if (target.nodeName == 'LI') {
			this.selectElement(target);
			this.clearResult();
		}
	}

	clearResult = () => {
		this.resultNode.innerHTML = '';
		this.resultNode.classList.add('hidden');
		this.result = [];
	}

	addElementsTo = (node) => {
		if (this.result) {
			this.resultNode.classList.remove('hidden');
			
			for(let i = 0; i < this.result.length; i++) {
				node.innerHTML += '<li>' + this.result[i] + '</li>';
			}
		}
	}

	selectElement = selectedElement => {
		if (selectedElement) {
			this.inputNode.value = selectedElement.innerText;
		}
	}

	debounce = (callback) => {
	let timeout;

		return (target) => {
			clearTimeout(timeout);
			timeout = setTimeout(() => {
				callback(target), 500
			})
		}
	}
}